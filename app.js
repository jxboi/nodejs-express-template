var express = require("express");
var cors = require('cors');
var app = express();

app.use(cors());

app.get("/url", (req, res, next) => {
 res.json(["Tony","Lisa","Michael","Ginger","Food"]);
});

app.get("/yolo", (req, res, next) => {
 res.json(["Play","Games","Is","Fun"]);
});


module.exports = app;
